
class Employee {

    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get getName() {
        return this._name
    }
    set setName(name) {
        this._name = name
    }

    get getAge() {
        return this._age
    }
    set setAge(age) {
        this._age = age
    }

    get getSalary() {
        return this._salary
    }
    set setSalary(salary) {
        this._salary = salary
    }
}

class Programmer extends Employee {

    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get getSalary() {
        return this._salary*3
    }
}

let p1 = new Programmer("Yar", 43, 5000, 3);
let p2 = new Programmer("Max", 33, 4000, 2);

